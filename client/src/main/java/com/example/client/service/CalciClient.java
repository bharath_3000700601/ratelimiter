package com.example.client.service;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.example.common.Values;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CalciClient {

  private static final Logger logger = LoggerFactory.getLogger(CalciClient.class);

  public String Connect_Server(Values value) throws IOException {
    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      HttpPost httpPost = new HttpPost("http://localhost:8080/calci");
      httpPost.setHeader("Accept", "application/json");
      httpPost.setHeader("Content-type", "application/json");
      float number1, number2;
      number1 = value.getNumber1();
      number2 = value.getNumber2();
      char operator = value.getOperator();
      String jsonresult = "{\"number1\":\"%f\",\"operator\":\"%c\",\"number2\":\"%f\"}";
      String json = String.format(jsonresult, number1, operator, number2);
      StringEntity stringEntity = new StringEntity(json);
      httpPost.setEntity(stringEntity);


      // Create a custom response handler
      ResponseHandler<String> responseHandler = response -> {
        int status = response.getStatusLine().getStatusCode();
        String message = response.toString();
        if (status >= 200 && status < 300) {
          HttpEntity entity = response.getEntity();
          return entity != null ? EntityUtils.toString(entity) : null;
        } else {
          logger.warn("Exception has occured");
          if (status == 429)
            throw new ClientProtocolException("RateLimiter suspended the call " + status);
          else if (status == 400)
            throw new ClientProtocolException("Invalid Operator " + operator + " " + status);
          else
           throw new ClientProtocolException("Division by 0 is Invalid " + status);
        }
      };
      logger.info("Establishing connection with server");
      String responseBody = httpclient.execute(httpPost, responseHandler);
      return responseBody;
    }

  }
}
