package com.example.common;

public class Values
	{

		float number1;
		char operator;
		float number2;

		public Values()
        {
        }
		public Values(float number1, char operator, float number2)
			{
				super();
				this.number1 = number1;
				this.operator = operator;
				this.number2 = number2;
			}

		public float getNumber1()
			{
				return number1;
			}


		public char getOperator()
			{
				return operator;
			}

		public float getNumber2()
			{
				return number2;
			}

	}
