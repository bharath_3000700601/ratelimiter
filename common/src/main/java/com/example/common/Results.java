package com.example.common;

public class Results {
	
	float number1;
	char operator;
	float number2;
	float result;
	
	public Results() {
		
	} 
	

	public Results(float number1, char operator, float number2, float result)
		{
			super();
			this.number1 = number1;
			this.operator = operator;
			this.number2 = number2;
			this.result = result;
		}


	public float getNumber1()
		{
			return number1;
		}


	public void setNumber1(float number1)
		{
			this.number1 = number1;
		}


	public char getOperator()
		{
			return operator;
		}


	public void setOperator(char operator)
		{
			this.operator = operator;
		}


	public float getNumber2()
		{
			return number2;
		}


	public void setNumber2(float number2)
		{
			this.number2 = number2;
		}


	public float getResult()
		{
			return result;
		}


	public void setResult(float result)
		{
			this.result = result;
		}


	

}
