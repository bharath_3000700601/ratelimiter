package com.example.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import com.example.server.service.CalculationManager;


@SpringBootApplication
public class ControllerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ControllerApplication.class, args);
  }

 /* @Bean
  public CalculationManager getRestTemplate() {
    return new CalculationManager();
  }*/
}
