package com.example.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.example.common.Results;
import com.example.common.Values;


@Service
public class CalculationManager {
  private static final Logger logger = LoggerFactory.getLogger(CalculationManager.class);

  public Results Operation(Values value) {
    Results results = new Results();
    if (value.getOperator() == '/') {
      logger.debug("The Operator is /");
      results.setResult(value.getNumber1() / value.getNumber2());
    }

    else if (value.getOperator() == '*') {
      logger.debug("The Operator is *");
      results.setResult(value.getNumber1() * value.getNumber2());
    } else if (value.getOperator() == '+') {
      logger.debug("The Operator is +");
      results.setResult(value.getNumber1() + value.getNumber2());
    } else if (value.getOperator() == '-') {
      logger.debug("The Operator is -");
      results.setResult(value.getNumber1() - value.getNumber2());
    }
    results.setNumber1(value.getNumber1());
    results.setNumber2(value.getNumber2());
    results.setOperator(value.getOperator());
    logger.info("Operation " + value.getNumber1() + value.getOperator() + value.getNumber2()
        + " is calculated and the Result is " + results.getResult());
    return results;
  }

}
