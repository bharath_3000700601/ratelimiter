package com.example.server;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.common.Results;
import com.example.common.Values;
import com.example.server.exception.OperatorNotFoundException;
import com.example.server.service.CalculationManager;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;

@RestController
public class Calci {
  private static final Logger logger = LoggerFactory.getLogger(Calci.class);

  @Autowired
  CalculationManager calculationmanager;

  @RequestMapping(method = RequestMethod.POST, value = "/calci")
  @RateLimiter(name = "service2")
  public Results operation(@RequestBody Values value)
      throws OperatorNotFoundException, io.github.resilience4j.ratelimiter.RequestNotPermitted {

    Results results = new Results();
    if (((value.getOperator() != '/') && (value.getOperator() != '+')
        && (value.getOperator() != '-') && (value.getOperator() != '*'))) {
      logger.error("Invalid operator");
      throw new OperatorNotFoundException(value.getOperator());
    } else if (value.getNumber2() == 0 && value.getOperator() == '/') {
      logger.warn("Division by 0 is Invalid");
      throw new ArithmeticException("Division by 0 is Invalid");
    } else {
      results = calculationmanager.Operation(value);
      return results;
    }

  }
}
