package com.example.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class OperatorNotFoundException extends Exception {
  public OperatorNotFoundException(char operator) {
    super(operator + " is invalid operator");
  }
}
