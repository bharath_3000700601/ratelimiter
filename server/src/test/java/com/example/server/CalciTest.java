package com.example.server;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Test;
import com.example.common.Results;
import com.example.common.Values;
import com.example.server.exception.OperatorNotFoundException;


class CalciTest {

  @Test
  void testAdditon() throws OperatorNotFoundException {

    Calci calci = mock(Calci.class);
    Values value = new Values(5, '+', 10);
    when(calci.operation(value)).thenReturn(new Results(5, '+', 10, 15));
    assertEquals(15, calci.operation(value).getResult());
  }

  @Test
  void testSubstraction() throws OperatorNotFoundException {
    Calci calci = mock(Calci.class);
    Values value = new Values(50, '-', 10);
    when(calci.operation(value)).thenReturn(new Results(50, '-', 10, 40));
    assertEquals(40, calci.operation(value).getResult());
  }

  @Test
  void testDivision() throws OperatorNotFoundException {
    Calci calci = mock(Calci.class);
    Values value = new Values(50, '/', 10);
    when(calci.operation(value)).thenReturn(new Results(50, '/', 10, 5));
    assertEquals(5, calci.operation(value).getResult());
  }

  @Test
  void testMultiplication() throws OperatorNotFoundException {
    Calci calci = mock(Calci.class);
    Values value = new Values(5, '*', 10);
    when(calci.operation(value)).thenReturn(new Results(5, '*', 10, 50));
    assertEquals(50, calci.operation(value).getResult());
  }

  @Test
  void testInValidOperatorException() throws OperatorNotFoundException {
    Calci calci = new Calci();
    Values value = new Values(5, ')', 0);
    assertThrows(OperatorNotFoundException.class, () -> calci.operation(value));
  }

  @Test
  void testArithmeticException() throws OperatorNotFoundException {
    Calci calci = new Calci();
    Values value = new Values(5, '/', 0);
    assertThrows(ArithmeticException.class, () -> calci.operation(value));
  }

}
